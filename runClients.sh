#!/bin/bash


timer=0
curr=0

while true;
do
	nb1=`expr $RANDOM / 6000`
	timer=`expr $timer + $nb1`
	if (( $timer >= 60 ))
	then
		curr=`expr \( $curr + 1 \) % 3`
		timer=0
	fi


	max=`expr $curr \* 5 + 1`
	for i in `seq 0 $max`
	do
		if (( $curr < 2 ))
		then
			python3 workinator_client.py &
		fi
	done
	sleep $nb1
done
