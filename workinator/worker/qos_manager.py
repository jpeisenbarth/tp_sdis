import time
import pika
from workinator.worker.worker import Worker
from threading import Thread


class QoSManager(Thread):
    """Class that represents the Quality of service manager"""

    LOW_LIMIT = 2
    HIGH_LIMIT = 10

    IDLE_LIMIT_TIME = 5
    LOW_LIMIT_TIME = 2
    HIGH_LIMIT_TIME = 5

    DECREASE_IDLE = 0.75  # when idle limit is reached -> -0.75% worker
    INCREASE_LOW = 1  # when low limit is reached -> +100% worker
    INCREASE_HIGH = 3  # when hish limit is reached -> +300% worker

    MAX_WORKER = 50

    def __init__(self):
        """Build the Quality of Service"""
        Thread.__init__(self)
        self.terminated = False
        self.max = 0
        self.messageCount = 0
        self.idle_time = 0
        self.low_limit_time = 0
        self.high_limit_time = 0
        self.workerList = []
        self.toDeleteWorker = []
        # run in thread
        w = Worker(id="w0")
        self.workerList.append(w)
        w.run()

    def getMax(self):
        self.max += 1
        return self.max

    def countRunningWorker(self):
        """Return the number of running workers (the ones which are running a task)"""
        count = 0
        for w in self.workerList:
            if w.haveTask():
                count += 1
        return count

    def countWorker(self):
        """Return the number of active workers"""
        return len(self.workerList)

    def createWorker(self, n=1):
        """Create a new worker"""
        length = len(self.workerList)
        for i in range(length, length+n):
            if len(self.workerList) < QoSManager.MAX_WORKER:
                w = Worker(id="w"+str(i))
                self.workerList.append(w)
                w.run()

    def removeWorker(self, n=1):
        """Tag a worker with the tag "to stop" """
        length = len(self.workerList)
        for i in range(length-1, length-1-n, -1):
            if(i <= 0):
                break
            self.workerList[i].stop()
            self.toDeleteWorker.append(self.workerList[i])
            del self.workerList[i]

    def deleteQuitWorker(self):
        """Delete the worker that are stopped"""
        length = len(self.toDeleteWorker)
        for i in range(length-1, -1, -1):
            if not self.toDeleteWorker[i].haveTask():
                self.toDeleteWorker[i].quit()
                del self.toDeleteWorker[i]

    def countTask(self):
        """Returns the number of messages in the queue"""
        connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))
        channel = connection.channel()
        return channel.queue_declare(queue="task_queue", durable=True, exclusive=False, auto_delete=False).method.message_count

    def stop(self):
        """Stop the quality of service and remove all active workers"""
        self.removeWorker(len(self.workerList))
        self.terminated = True

    def run(self):
        """Start the Quality of Service algorithm"""
        connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))
        channel = connection.channel()
        while not self.terminated:
            nbWorker = self.countWorker()
            runningWorker = self.countRunningWorker()
            stopWorker = nbWorker - runningWorker
            # TODO Go out the loop if it's possible
            self.message_count = channel.queue_declare(queue="task_queue", durable=True, exclusive=False, auto_delete=False).method.message_count

            print("msg_count: {}; nb_workers: {}; running_workers: {}; " \
                  "workers_to_delete: {}".format(self.message_count, nbWorker,
                                                 runningWorker, len(self.toDeleteWorker)))

            # update times
            if(self.message_count >= QoSManager.LOW_LIMIT):
                self.low_limit_time += 1
            if(self.message_count >= QoSManager.HIGH_LIMIT):
                self.high_limit_time += 1
            if(self.countWorker() >= self.countRunningWorker()):
                self.idle_time += 1

            if self.high_limit_time > QoSManager.HIGH_LIMIT_TIME:
                self.createWorker(len(self.workerList)*QoSManager.INCREASE_HIGH)
                self.high_limit_time = 0
                self.low_limit_time = 0

            elif(self.low_limit_time > QoSManager.LOW_LIMIT_TIME):
                self.createWorker(len(self.workerList)*QoSManager.INCREASE_LOW)
                self.low_limit_time = 0

            # check load of system
            elif(self.idle_time > QoSManager.IDLE_LIMIT_TIME):
                self.removeWorker(int(stopWorker*QoSManager.DECREASE_IDLE))
                self.idle_time = 0

            self.deleteQuitWorker()

            # wait next step
            time.sleep(1)
