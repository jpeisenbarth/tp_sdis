import pylab
import time
from threading import Thread


class QoSManagerView():
    """ Class that represents the GUI displaying the statistics of the Quality of Services Manager"""

    def __init__(self, qos):
        """Build the view of the Quality of service"""
        self.qos = qos
        self.terminated = False
        xAchse = pylab.arange(0, 100, 1)
        yAchse = pylab.array([0]*100)

        self.fig = pylab.figure(1)
        ax = self.fig.add_subplot(111)
        ax.grid(True)
        ax.set_title("Realtime Waveform Plot")
        ax.set_xlabel("Time")
        ax.set_ylabel("Number of worker")
        ax.axis([0, 100, -1, 50])
        self.line1 = ax.plot(xAchse, yAchse, 'b-')
        self.ax = ax

        ax2 = self.fig.add_subplot(111)
        ax2.grid(True)
        ax2.set_title("Realtime Waveform Plot")
        ax2.set_xlabel("Time")
        ax2.set_ylabel("Number of waiting task")
        ax2.axis([0, 100, -1, 50])
        self.line2 = ax2.plot(xAchse, yAchse, 'r-')
        self.ax2 = ax2

        ax3 = self.fig.add_subplot(111)
        ax3.grid(True)
        ax3.set_title("Realtime Waveform Plot")
        ax3.set_xlabel("Time")
        ax3.set_ylabel("Number of running worker")
        ax3.axis([0, 100, -1, 50])
        self.line3 = ax3.plot(xAchse, yAchse, 'g-')
        self.ax3 = ax3

        self.manager = pylab.get_current_fig_manager()

        self.values = []
        self.values = [0 for x in range(100)]

        self.values2 = []
        self.values2 = [0 for x in range(100)]

        self.values3 = []
        self.values3 = [0 for x in range(100)]

    def RealtimePloter(self):
        """ Start the infinite loop that displays the plot"""
        # self.values.append(self.qos.countWorker())
        while not self.terminated:
            nb_worker = self.qos.countWorker()
            nb_task = self.qos.countTask()
            nb_running_worker = self.qos.countRunningWorker()
            self.values.append(nb_worker)
            self.values2.append(nb_task)
            self.values3.append(nb_running_worker)
            CurrentXAxis = pylab.arange(len(self.values)-100, len(self.values), 1)
            self.line1[0].set_data(CurrentXAxis, pylab.array(self.values[-100:]))
            CurrentXAxis2 = pylab.arange(len(self.values2)-100, len(self.values2), 1)
            self.line2[0].set_data(CurrentXAxis2, pylab.array(self.values2[-100:]))
            CurrentXAxis3 = pylab.arange(len(self.values3)-100, len(self.values3), 1)
            self.line3[0].set_data(CurrentXAxis3, pylab.array(self.values3[-100:]))
            maximum = 50 if nb_worker < 45 else nb_worker+5
            self.ax.axis([CurrentXAxis.min(), CurrentXAxis.max(), -1, maximum])
            self.manager.canvas.draw()
            time.sleep(1)
        # manager.show()

    def stop(self):
        """ Stop the displaying of the plot and exit the infinite loop"""
        self.terminated = True
        pylab.close()
        print("Fin GUI")

    def run(self):
        """Start the plotting"""
        t = Thread(target=self.RealtimePloter)
        t.start()
        pylab.show()
        self.stop()


# v = QoSManagerView(None)
# v.run()
