import pika
import time

from threading import Thread


class Worker():
    """A class that represents a Worker"""

    def __init__(self, running=True, id=""):
        """Build a worker"""
        self.running = running
        self.have_task = False
        self.id = id
        self.quit_var = False

    def getId(self):
        """Return the Id of the worker"""
        return self.id

    def haveTask(self):
        """Return True if the worker has a task to run, False otherwise"""
        return self.have_task

    def run(self):
        """Get the task from the queue and run it"""
        # print(" [++]["+self.id+"] running")
        def callback(ch, method, properties, body):
            ch.basic_ack(delivery_tag=method.delivery_tag)
            response = body.decode('utf-8').split(" ")
            # print(" [<<]["+self.id+"] Receive message --> id : "+response[0]+", n : "+response[1])
            self.runTask(response[0], response[1])
            if not self.running:
                self.quit()


        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = connection.channel()

        self.channel.queue_declare(queue='task_queue', durable=True)

        # while self.running:
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(callback, queue='task_queue')

        self.t = Thread(target=self.channel.start_consuming)
        self.t.start()

    def quit(self):
        """Called after a worker has just finished to run its task."""
        self.channel.stop_consuming()
        # print(" [^^]["+self.id+"] Quit")
        self.quit_var = True

    def isQuit(self):
        """Return True is the worker must quit, False otherwise"""
        return self.quit_var

    def stop(self):
        """Stops the worker"""
        self.running = False

    def runTask(self, id, n):
        """Send in the queue the result of the task that has been run."""
        self.have_task = True
        # print(" [<>]["+self.id+"] Running for "+str(n)+" seconds")
        time.sleep(int(n))

        # print(" [--]["+self.id+"] End")

        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.exchange_declare(exchange='response', type='fanout')

        message = id+" response("+self.id+")"
        channel.basic_publish(exchange='response', routing_key='', body=message)
        # print(" [>>]["+self.id+"] Sent %r\n" % (message,))
        connection.close()
        self.have_task = False
