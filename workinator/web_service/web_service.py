import pika
from flask import Flask, request, abort
import logging

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app = Flask("Workinator")


class WebService():
    """Class that represents the web service"""
    connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='task_queue', durable=True)

    def __init__(self):
        pass

    def run(self):
        """Launch the app"""
        app.run(threaded=True)

    @app.route('/workinator', methods=['GET'])
    def workinator():
        """Callback function\n
        Routing type : /workinator?myid=<string>&value=<int>\n
        Return the ip address where the queue is hosted"""
        myid, value = request.args.get('myid'), request.args.get('value')
        if not myid or not value:
            abort(400)
        try:
            # Check if int
            value = int(value)
        except:
            abort(400)

        message = myid+" "+str(value)
        WebService.channel.basic_publish(exchange='',
                                   routing_key='task_queue',
                                   body=message,
                                   properties=pika.BasicProperties(delivery_mode=2))
        print(" [WS] GET %r" % (value,))
        return "localhost"
