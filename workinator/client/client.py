import requests
import pika


class Client:
    """Class that represents a client"""

    def __init__(self, id, value):
        """Build a Client with the specified id and the value to give a worker (time to wait)"""
        self.id = id
        self.value = value
        print("client : {} : {}".format(self.id, str(self.value)))

    def run(self):
        """Makes a request to the web service"""
        url = "http://localhost:5000/workinator?myid={}&value={}".format(self.id, str(self.value))
        response = requests.get(url)
        # TODO check 200 or not
        self.urlResponse = response.text

    def waitResponse(self):
        """Listens to the queue for a response"""
        # define a callback for channel
        def callback(ch, method, properties, body):
            # Get response
            response = body.decode('utf-8').split(" ")
            # Get response for my ID
            if response[0] == self.id:
                print("response : -> {}".format(response[1]))
                # Quit queue
                ch.stop_consuming()

        # Connection to the RMQ server
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.urlResponse))
        channel = connection.channel()

        # wait response from 'response' queue
        channel.exchange_declare(exchange='response',
                                 type='fanout')

        result = channel.queue_declare(exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange='response', queue=queue_name)

        print("Waiting response from '{}'".format(self.urlResponse))

        # start consume queue
        channel.basic_consume(callback, queue=queue_name, no_ack=True)
        channel.start_consuming()

        # close channel when quit queue
        channel.close()
