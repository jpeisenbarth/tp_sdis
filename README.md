
# Workinator


## Auteurs
Jean-Philippe Eisenbarth

Valentin Giannini

## Rapport

Le rapport, au format PDF, peut être téléchargé via l'onglet Downloads de BitBucket.

## Dépendances

- Python 3
- Requests
- Pika (RabbitMQ client)
- Flask (+ Werkzeug + itsdangerous + Jinja2 + MarkupSafe)

Manière simple de répondre au dépendances : pip !
[Get pip](https://pip.pypa.io/en/stable/installing/)

Pour les installer localement : pip install -r --user requirements.txt

Pour les installer pour tout le système (system-wide) : pip install -r requirements.txt (en tant qu'admin)

- Matplotlib

Pour installer Matplotlib, utiliser votre package manager : [sudo] apt-get install python3-matplotlib

Sur Windows, voir ici : https://pypi.python.org/pypi/matplotlib/

## Utilisation :

Lancement du serveur
-> ./WorkinatorServer.py

Lancement d'un client
-> ./WorkinatorClient.py

Lancement des clients de manière aléatoire
-> ./runClients.sh

## Documentation

Pour consulter la documentation, ouvrez avec un navigateur internet le fichier situé dans docs/build/html/index.html.
