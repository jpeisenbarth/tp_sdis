#!/usr/bin/env python3

from threading import Thread
import os as os
import workinator.web_service.web_service as web_service
import workinator.worker.qos_manager as qos_manager
import workinator.worker.qos_manager_view as qos_manager_view

qos_manager = qos_manager.QoSManager()
qos_manager.start()

qos_view = qos_manager_view.QoSManagerView(qos_manager)


ws = Thread(target=web_service.WebService().run)
ws.start()

# Blocking call -> GUI
qos_view.run()
# Now end of GUI -> quit all

qos_manager.stop()
os._exit(0)
