#! /usr/bin/env python3

import random
import workinator_client
import multiprocessing
import time

while(1):
    rand = random.randrange(6)
    print("Running client {}".format(rand))
    multiprocessing.Process(target=workinator_client.run).start()
    time.sleep(rand)
