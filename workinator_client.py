#!/usr/bin/env python3

from random import randint
import workinator.client.client as client
import datetime


def run():
    id = "client"+str(randint(1, 1000))
    time = randint(10, 30)

    start = datetime.datetime.now()
    c = client.Client(id, time)
    c.run()
    c.waitResponse()
    end = datetime.datetime.now()

    print("End -> duration : {}, initial : {}, waiting : {}".format((end-start),
          time,
          ((end-start)-datetime.timedelta(seconds=time))))

if __name__ == '__main__':
    run()
