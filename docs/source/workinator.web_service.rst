workinator.web_service package
==============================

Submodules
----------

workinator.web_service.web_service module
-----------------------------------------

.. automodule:: workinator.web_service.web_service
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: workinator.web_service
    :members:
    :undoc-members:
    :show-inheritance:
